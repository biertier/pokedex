//
//  Constants.swift
//  Pokedex
//
//  Created by Tobias Biermeier on 6/11/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()

